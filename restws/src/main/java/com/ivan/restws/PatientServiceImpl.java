package com.ivan.restws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.ivan.restws.exceptions.PatientBussinessException;
import com.ivan.restws.model.Patient;

@Service
public class PatientServiceImpl implements PatientService {
	
	private long idNumber = 1 ; 
	Map<Long , Patient> patients = new HashMap<>();
	
	
	public PatientServiceImpl()
	{
		init();
	}
	
	private void init()
	{
		Patient patient1 = new Patient();
		patient1.setId(idNumber);
		patient1.setName("Darin");
		patients.put(idNumber, patient1);

	}

	@Override
	public List<Patient> getPatients() {
		Collection<Patient> results = patients.values();
		ArrayList<Patient> response = new ArrayList<Patient>(results);
		return response ;
	}

	@Override
	public Patient getPatient(long id) {
		if(patients.get(id) == null)
		{
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return patients.get(id);
	}

	@Override
	public Response createPatient(Patient patient) {
		patient.setId(++idNumber);
		patients.put(idNumber, patient);
		return Response.ok(patient).build();
	}

	@Override
	public Response updatePatient(Patient patient) {
		
		Response response = null ; 
		Patient currentPatient = patients.get(patient.getId());
		if(currentPatient != null)
		{
			patients.put(patient.getId(), patient);
			response =  Response.ok().build();
		}
		else
		{
			throw new PatientBussinessException();
		}
		
		
		
		return response;
		
	}

	@Override
	public Response deletePatient(long id) {
		Patient currentPatient = patients.get(id);
		if(currentPatient != null)
		{
			patients.remove(id);
			return Response.ok().build();
		}
		return Response.notModified().build();
	}
	

}
