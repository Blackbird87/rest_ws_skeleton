package com.ivan.restws.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.ivan.restws.exceptions.PatientBussinessException;

@Provider
public class PatientBusinessExceptionMapper implements ExceptionMapper<PatientBussinessException>{

	@Override
	public Response toResponse(PatientBussinessException e) {
	
		return Response.status(Status.NOT_FOUND).build();
	}

}
